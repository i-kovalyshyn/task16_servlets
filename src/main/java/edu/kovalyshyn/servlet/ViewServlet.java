package edu.kovalyshyn.servlet;

import edu.kovalyshyn.Order;
import edu.kovalyshyn.dao.OrderDao;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.ServerException;
import java.util.List;

@WebServlet("/ViewServlet")
public class ViewServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServerException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<a href = 'index.jsp'> Add new Order</a>");
        out.println("<h1>Orders List</h1>");

        List<Order> list = OrderDao.getAllOrders();

        out.print("<table border = '1' width = '100%'");
        out.print("<tr><th>Id</th><th>Name</th><th>PhoneNumber</th><th>Email</th><th>Pizza</th><th>Edit</th><th>Delete</th></tr>");
        for (Order order : list){
            out.print("<tr><td>"+order.getId()+"</td><td>"
                    +order.getName()+"</td><td>"
                    +order.getPhoneNumber()+"</td><td>"
                    +order.getEmail()+"</td><td>"
                    +order.getPizza()+"</td><td><a href='EditServlet?id="
                    +order.getId()+"'>edit</a></td><td><a href='DeleteServlet?id="
                    +order.getId()+"'>delete</a></td></tr>");
        }
        out.print("</table>");
        out.close();
    }
}
