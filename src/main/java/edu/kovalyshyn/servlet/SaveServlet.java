package edu.kovalyshyn.servlet;

import edu.kovalyshyn.Order;
import edu.kovalyshyn.dao.OrderDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/SaveServlet")
public class SaveServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        String name = request.getParameter("name");
        String phoneNumber = request.getParameter("phoneNumber");
        String email = request.getParameter("email");
        String pizza = request.getParameter("pizza");

        Order order = new Order();
        order.setName(name);
        order.setPhoneNumber(phoneNumber);
        order.setEmail(email);
        order.setPizza(pizza);

        int status = OrderDao.save(order);
        if(status>0){
            out.print("<p> Record saved successfully ! </p>");
            request.getRequestDispatcher("index.jsp").include(request,response);
        }else {
            out.println("Sorry! unable to save record");
        }
    }
}
