package edu.kovalyshyn.servlet;

import edu.kovalyshyn.dao.OrderDao;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.rmi.ServerException;

@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServerException, IOException {
        String sid = request.getParameter("id");
        int id = Integer.parseInt(sid);
        OrderDao.delete(id);
        response.sendRedirect("ViewServlet");
    }
}
