package edu.kovalyshyn.servlet;

import edu.kovalyshyn.Order;
import edu.kovalyshyn.dao.OrderDao;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.ServerException;

@WebServlet("/EditServlet")
public class EditServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServerException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<h1>Update Order<h1>");
        String sid = request.getParameter("id");
        int id = Integer.parseInt(sid);
        Order order = OrderDao.getOrderById(id);

        out.print("<form action='EditServlet2' method='post'>");
        out.print("<table>");
        out.print("<tr><td></td><td><input type='hidden' name='id' value='"
                +order.getId()+"'/></td></tr>");
        out.print("<tr><td>Name:</td><td><input type='text' name='name' value='"
                +order.getName()+"'/></td></tr>");
        out.print("<tr><td>PhoneNumber:</td><td><input type='text' name='phoneNumber' value='"
                +order.getPhoneNumber()+"'/></td></tr>");
        out.print("<tr><td>Email:</td><td><input type='email' name='email' value='"
                +order.getEmail()+"'/></td></tr>");
        out.print("<tr><td>Pizza:</td><td>");
        out.print("<select name='pizza' style='width:150px'>");
        out.print("<option>Cheese pizza</option>");
        out.print("<option>Vegan pizza</option>");
        out.print("<option>Carbonara pizza</option>");
        out.print("<option>Other</option>");
        out.print("</select>");
        out.print("</td></tr>");
        out.print("<tr><td colspan='2'><input type='submit' value='Edit &amp; Save '/></td></tr>");
        out.print("</table>");
        out.print("</form>");

        out.close();
    }
}
