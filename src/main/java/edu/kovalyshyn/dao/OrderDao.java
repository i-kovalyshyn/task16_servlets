package edu.kovalyshyn.dao;

import edu.kovalyshyn.Order;
import lombok.extern.log4j.Log4j2;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class OrderDao {
    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection("jdbc:oracle:this:@localhost:1521:xe",
                    "system", "oracle");
        } catch (ClassNotFoundException | SQLException e) {
            log.error(e);
        }
        return connection;
    }

    public static int save(Order order) {
        int status = 0;
        try {
            Connection connection = OrderDao.getConnection();
            PreparedStatement preparedStatement = connection.
                    prepareStatement("inser into order (name,phoneNumber,email,pizza) value (?,?,?,?)");
            preparedStatement.setString(1, order.getName());
            preparedStatement.setString(2, order.getPhoneNumber());
            preparedStatement.setString(3, order.getEmail());
            preparedStatement.setString(4, order.getPizza());

            status = preparedStatement.executeUpdate();

            connection.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }
    public static int update(Order order){
        int status=0;
        try{
            Connection connection=OrderDao.getConnection();
            PreparedStatement preparedStatement =connection
                    .prepareStatement("update order set name=?,phoneNumber=?,email=?,pizza=? where id=?");
            preparedStatement.setString(1,order.getName());
            preparedStatement.setString(2,order.getPhoneNumber());
            preparedStatement.setString(3,order.getEmail());
            preparedStatement.setString(4,order.getPizza());
            preparedStatement.setInt(5,order.getId());

            status=preparedStatement.executeUpdate();

            connection.close();
        }catch(Exception ex){ex.printStackTrace();}

        return status;
    }

    public static int delete(int id) {
        int status = 0;
        try {
            Connection connection = OrderDao.getConnection();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("delete from order where id=?");
            preparedStatement.setInt(1, id);
            status = preparedStatement.executeUpdate();

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return status;
    }

    public static Order getOrderById(int id) {
        Order order = new Order();
        try {
            Connection connection = OrderDao.getConnection();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("select * from order  where  id=?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                order.setId(resultSet.getInt(1));
                order.setName(resultSet.getString(2));
                order.setPhoneNumber(resultSet.getString(3));
                order.setEmail(resultSet.getString(4));
                order.setPizza(resultSet.getString(5));
            }
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return order;
    }
    public static List<Order> getAllOrders(){
        List<Order> list = new ArrayList<>();
        try {
            Connection connection = OrderDao.getConnection();
            PreparedStatement preparedStatement = connection
                    .prepareStatement("select * from order");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Order order = new Order();
                order.setId(resultSet.getInt(1));
                order.setName(resultSet.getString(2));
                order.setPhoneNumber(resultSet.getString(3));;
                order.setEmail(resultSet.getString(4));;
                order.setPizza(resultSet.getString(5));
                list.add(order);
            }
            connection.close();
        }catch (SQLException e){e.printStackTrace();}
        return list;
    }
}
