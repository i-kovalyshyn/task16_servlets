
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>task16_servlets</title>
  </head>
  <body>
  <h1>Form to order pizza</h1>
  <form action="SaveServlet" method="post">
    <table>
      <tr><td>Name:</td><td><input type="text" name="name"/></td></tr>
      <tr><td>Phone:</td><td><input type="text" name="phoneNumber"/></td></tr>
      <tr><td>Email:</td><td><input type="email" name="email"/></td></tr>
      <tr><td>Pizza:</td><td>
        <select name="pizza" style="width:150px">
          <option value="cheese">Cheese pizza</option>
          <option value="vegan">Vegan pizza</option>
          <option value="carbonara">Carbonara pizza</option>
          <option>Other</option>
        </select>
      </td></tr>
      <tr><td colspan="2"><input type="submit" value="Save Order"/></td></tr>
    </table>
  </form>

  <br/>
  <a href="ViewServlet">view orders</a>

  </body>
</html>
